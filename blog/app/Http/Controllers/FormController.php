<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function daftar(){
        return view('page.form');
    }

    public function kirim(request $request){
        $first_name = $request['first_name'];
        $last_name = $request['last_name'];

        return view('page.welcome',compact('first_name','last_name'));
    }
}
