<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        First name: <br><br>
        <input type="text" name="first_name"><br><br>
        Last name: <br><br>
        <input type="text" name="last_name"><br><br>
        Gender: <br><br>
        <input type="radio" name="gender" value="M"> Male <br>
        <input type="radio" name="gender" value="F"> Female <br>
        <input type="radio" name="gender" value="O"> Other <br><br>
        Nationality: <br><br>
        <select name="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="america">American</option>
            <option value="english">English</option>
        </select><br><br>
        Language Spoken: <br><br>
        <input type="checkbox" name="language" value="indonesia">Bahasa Indonesia <br>
        <input type="checkbox" name="language" value="english">English <br>
        <input type="checkbox" name="language" value="other">Other <br><br>
        Bio: <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <button type="submit">Sign Up</button>
    </form>
</body>
</html>