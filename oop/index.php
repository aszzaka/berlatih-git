<?php
require_once "animal.php";
require_once "frog.php";
require_once "ape.php";

$sheep = new Animal("shaun");
echo "Name : " . $sheep->name . "<br>";
echo "legs : " . $sheep->legs . "<br>";
echo "cold blooded : " . $sheep->cold_blooded . "<br><br>";

$katak = new Frog("buduk");
echo "Name : " . $katak->name . "<br>";
echo "legs : " . $katak->legs . "<br>";
echo "cold blooded : " . $katak->cold_blooded . "<br>";
echo $katak->jump() . "<br><br>";

$kera = new Ape("kera sakti");
echo "Name : " . $kera->name . "<br>";
echo "legs : " . $kera->legs . "<br>";
echo "cold blooded : " . $kera->cold_blooded . "<br>";
echo $kera->yell() . "<br>";

?>